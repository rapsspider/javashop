/*
 * Default.java                                 23 nov. 2015
 * 3IL3 DOO-GL 2015-2016
 */
package app.controllers;

import javax.servlet.http.HttpServletRequest;

import cci.corps.Controller;

/**
 * TODO comment class responsibility
 * @author Administrateur
 *
 */
public class Default extends Controller {
	
    /**
	 * TODO comment state construction
	 * @param request
	 */
	public Default(HttpServletRequest request) {
		super(request);
	}

	/**
     * TODO comment method role
     * @return Le nom de la vue � utiliser
     */
    public String index() {
    	System.out.println("Bienvenue dans mon action  : " + this.request.getRequestURI());
    	//this.view.setName("Bienvenue dans mon action  : " + this.request.getRequestURI());
    	return "Default/index";
    }
    
    /**
     * TODO comment method role
     * @return Le nom de la vue � utiliser
     */
    public String home() {
    	System.out.println("Bienvenue dans la page d'accueil : " + this.request.getRequestURI());
    	//this.view.setName("Bienvenue dans la page d'accueil : " + this.request.getRequestURI());
    	return "Default/home";
    }
}

/*
 * Routeur.java                                 23 nov. 2015
 * 3IL3 DOO-GL 2015-2016
 */
package cci.corps;

import java.util.Iterator;
import java.util.Vector;

/**
 * TODO comment class responsibility
 * @author Administrateur
 *
 */
public class Router {

	protected Vector<Route> routes;
	
	/**
	 * TODO comment state construction
	 */
	public Router() {
		this.routes = new Vector<Route>();
		this.initialize();
	}
	
	/**
	 * TODO comment method role
	 */
	public void initialize() {
		this.routes.addElement(new Route("/index"));
		this.routes.addElement(new Route("/*", "app.controllers.Default", "home"));
	}

	/**
	 * TODO comment method role
	 * @param path
	 * @return La route correspondant au path.
	 */
	public Route find(String path) {
		boolean         trouve;
		Route           route;
		Iterator<Route> iterator = this.routes.iterator();
		
		do {
			route  = (Route) iterator.next();
			trouve = route.isCorrectPath(path);
		} while(!trouve && iterator.hasNext());
		
		return trouve ? route : null;
	}
}

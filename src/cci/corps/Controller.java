/*
 * Controller.java                                 23 nov. 2015
 * 3IL3 DOO-GL 2015-2016
 */
package cci.corps;

import javax.servlet.http.HttpServletRequest;

/**
 * TODO comment class responsibility
 * @author Administrateur
 *
 */
public class Controller {
	
	protected HttpServletRequest request;
	
	/**
	 * TODO comment state construction
	 */
	public Controller() {
		this(null);
	}
	
	/**
	 * TODO comment state construction
	 * @param req 
	 * @param view 
	 */
	public Controller(HttpServletRequest req) {
		this.request = req;
	}
	
	/**
	 * Setter sur la requ�te.
	 * @param req 
	 */
	public void setRequest(HttpServletRequest req) {
		this.request = req;
	}
}

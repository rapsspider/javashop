/*
 * Route.java                                 23 nov. 2015
 * 3IL3 DOO-GL 2015-2016
 */
package cci.corps;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TODO comment class responsibility
 * @author Administrateur
 *
 */
public class Route {
    protected String path;
    
    protected String controller;
    
    protected String action;
    
    /**
     * TODO comment state construction
     * @param p
     * @param c
     * @param a
     */
    public Route(String p) {
    	this(p, "app.controllers.Default", "index");
    }
    
    /**
     * TODO comment state construction
     * @param p
     * @param c
     * @param a
     */
    public Route(String p, String c) {
    	this(p, c, "index");
    }
    
    /**
     * TODO comment state construction
     * @param p
     * @param c
     * @param a
     */
    public Route(String p, String c, String a) {
    	this.path = p;
    	this.controller = c;
    	this.action = a;
    }
    
    /**
     * TODO comment method role
     * @param path
     * @return VRAI si le path est le m�me.
     */
    public boolean isCorrectPath(String path) {
		Pattern         pattern;
		Matcher         matcher;
		
    	if(path == null || path == "") {
    		path = "/";
    	}
    	System.out.println(this.path + " | " + path);
		pattern = Pattern.compile(this.path);
		matcher = pattern.matcher(path);
    	
    	return matcher.find();
    }

	/**
	 * TODO comment method role
	 * @return Le contr�leur associ� � cette route.
	 */
	public String getController() {
		return this.controller;
	}
	
	/**
	 * TODO comment method role
	 * @return L'action associ� � cette route.
	 */
	public String getAction() {
		return this.action;
	}
}

package cci.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cci.corps.Controller;
import cci.corps.Route;
import cci.corps.Router;

/**
 * Servlet implementation class Hello
 */
@WebServlet("/")
public class DispatcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    /** TODO comment field role */
    public final static String PATH = "WEB-INF/jsp/";
    
	private final Router router;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DispatcherServlet() {
        super();
        this.router = new Router();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Response
		response.setContentType("text/html");
		this.execute(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	/**
	 * Execute le dispatcher afin de lancer le traitement de la requ�te dans
	 * l'architecture MVC.
	 * 
	 * Dans un premier temps, il est necessaire de trouver la route correspondant
	 * � la requ�te soumise par le client.
	 * Ensuite, la route nous permet d'identifier le contr�leur et l'action afin
	 * de remplir le corps de la r�ponse avec la vue correspondante.
	 * 
	 * @param request
	 * @param response
	 */
	public void execute(HttpServletRequest request, HttpServletResponse response) {
		String         path;
		Route          route;
		Class<?>       controllerClass;
		Controller     controller;
		Constructor<?> constructor;
		PrintWriter    out;
		Method         action;
		Object         view;
		RequestDispatcher dispatch;
		
		path  = request.getRequestURI();
		path  = path.substring(request.getContextPath().length());
		route = this.router.find(path);
		
		// Si la route existe
		if(route == null) {
			response.setStatus(404);
			return;
		}
		
		try {
			// On r�cup�re une instance du contr�leur
			controllerClass = Class.forName(route.getController());
			constructor     = controllerClass.getConstructor(
				HttpServletRequest.class
			);
			controller = (Controller) constructor.newInstance(request);
			
		    // On r�cup�re l'action � lancer (M�thode du contr�leur).
			action     = controllerClass.getMethod(route.getAction());
			// On lance l'action et on r�cup�re la vue.
		    view = action.invoke(controller);
		    // On r�cup�re le flux pour �crire dans la r�ponse.
			out        = response.getWriter();
			
			if(out != null) {
				// On �crit le r�sultat de la vue dans la r�ponse.
				dispatch = this.getServletContext().getRequestDispatcher("/" + PATH + view + ".jsp");
				
				if(dispatch != null) {
					dispatch.forward(request, response);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

/*
 * ContextListener.java                                 23 nov. 2015
 * 3IL3 DOO-GL 2015-2016
 */
package cci.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import cci.utils.Compteur;

/**
 * TODO comment class responsibility
 * @author Administrateur
 *
 */
@WebListener
public final class ContextListener implements ServletContextListener {

	private ServletContext context = null;
	
	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		context = arg0.getServletContext();
		context.setAttribute("Compteur",  new Compteur());
	}

}

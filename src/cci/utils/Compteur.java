/*
 * Compteur.java                                 23 nov. 2015
 * 3IL3 DOO-GL 2015-2016
 */
package cci.utils;

/**
 * TODO comment class responsibility
 * @author Administrateur
 *
 */
public class Compteur {
    private int compteur;
    
    /**
     * TODO comment state construction
     */
    public Compteur() {
    	compteur = 0;
    }

	/**
	 * @return the compteur
	 */
	public int getCompteur() {
		return compteur;
	}

	/**
	 * @param compteur the compteur to set
	 */
	public void setCompteur(int compteur) {
		this.compteur = compteur;
	}
    
	/**
	 * Inc�mrente le compteur.
	 * @return Le valeur de l'entier compteur.
	 */
	public synchronized int incCompteur() {
		return ++compteur;
	}
}
